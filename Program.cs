﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.IO;
using V83;


namespace tsdRelay
{
    // An interface class
    public abstract class RequestProcessor
    {
        public abstract bool Processing(String str);
        public DirectoryManager dirManager;
        public Socket soc;
        public String GUID;
    }


    public class PortManager
    {
        int MIN_PORT;
        int MAX_PORT;
        List<int> busy_ports;

        public PortManager(int min_port,int max_port)
        {
            MIN_PORT = min_port;
            MAX_PORT = max_port;
            busy_ports = new List<int>();
        }

        public bool NextFreePort(out int port)
        {
            if (busy_ports.Count == 0)
            {
                port = MIN_PORT;
                busy_ports.Add(MIN_PORT);
                return true;
            }
            else
            {
                int next_port = busy_ports.Last() + 1;
                if (next_port <= MAX_PORT)
                {
                    busy_ports.Add(next_port);
                    port = next_port;
                    return true;
                }
                else
                {
                    port = -1;
                    return false;
                }
            }
        }

        public bool FreePort(int port)
        {
            busy_ports.Remove(port);
            return true;
        }
    }

    public class ListDirectoryProcessor : RequestProcessor
    {
        public ListDirectoryProcessor(String GUID, Socket s, DirectoryManager dm)
        {
            soc = s;
            dirManager = dm;
            this.GUID = GUID;
        }

        public override bool Processing(String str)
        {
            String response = ":" + GUID +":";
            dirManager.ReadDirectories();
            foreach (DirectoryInfo dir in dirManager.subDirList)
            {
                response += dir.Name + ";";
            }
            byte[] data = Encoding.UTF8.GetBytes(response.Substring(0, response.Length - 1));
            soc.Send(data);
            //soc.Close();

            return true;
        }
    }

    public class CreateDirectoryProcessor : RequestProcessor
    {
        public CreateDirectoryProcessor(String GUID, Socket s, DirectoryManager dm)
        {
            soc = s;
            dirManager = dm;
            this.GUID = GUID;
        }

        public override bool Processing(String str)
        {
            char[] supp_el = new char[1];
            supp_el[0] = (char)0;
            str = str.Trim(supp_el);

            bool success = dirManager.CreateDirectory(str);
            if (success)
            {
                String response = ":" + GUID + ":";

                dirManager.ReadDirectories();
                foreach (DirectoryInfo dir in dirManager.subDirList)
                {
                    response += dir.Name + ";";
                }
                byte[] data = Encoding.UTF8.GetBytes(response.Substring(0, response.Length-1));
                soc.Send(data);
                //soc.Close();
                return true;
            }  else
            {
                return false;
            }

        }
    }


    public class GetFileProcessor : RequestProcessor
    {
        IPAddress ip;
        PortManager portmanager;
        int filesize;

        public GetFileProcessor(String GUID, Socket s, DirectoryManager dm, IPAddress ip_address = null, PortManager pm = null)
        {
            soc = s;
            dirManager = dm;
            this.GUID = GUID;
            this.ip = ip_address;
            portmanager = pm;
        }

        public override bool Processing(String str)
        {

            int port;
            portmanager.NextFreePort(out port);

            if (port == -1)
                return false;

            char[] supp_el = new char[1];
            supp_el[0] = ';';
            string[] input_list = str.Split(supp_el); // [0] - Folder, [1] - filename , [2] - size

            int size = Convert.ToInt32(input_list[2]);
            int current_size = 0;
            bool minus_four_byte_flag = true; 

            Socket local_soc = new Socket(ip.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            IPEndPoint ipEndPoint = new IPEndPoint(ip, port);
            local_soc.ReceiveBufferSize = 128000;
            local_soc.Bind(ipEndPoint);
            local_soc.Listen(2);

            String response = ":" + GUID + ":"+Convert.ToString(port);
            byte[] data = Encoding.UTF8.GetBytes(response.Substring(0, response.Length));
            soc.Send(data);
            soc.Close();


            Socket tsco = local_soc.Accept();
            local_soc.Close();

            String filepath;
            byte[] fileData;
            if (dirManager.getFilFullPath(input_list[0], input_list[1], out filepath))
            {
                using (FileStream fs = new FileStream(filepath, FileMode.CreateNew, FileAccess.Write))
                {
                    while (current_size < (size+4) )
                    {
                        Console.WriteLine("Buffered data size : {0}", tsco.Available);
                        if (tsco.Available != 0)
                        {
                            fileData = new byte[tsco.Available];
                            tsco.Receive(fileData);
                            if (minus_four_byte_flag)
                            {
                                fs.Write(fileData, 4, fileData.Length - 4);
                                minus_four_byte_flag = false;
                            } else
                            {
                                fs.Write(fileData, 0, fileData.Length);
                            }
                            current_size += fileData.Length;

                        }
                    }
                    fs.Close();
                }
                //tsco.Close();
                tsco.Close();
                portmanager.FreePort(port);
                return true;
            } else
            {
                tsco.Close();
                portmanager.FreePort(port);
                return false;
            }
            
        }
    }


    public class TSDException : Exception
    {

    }

    public class DirectoryManagerException : Exception
    {
        public String text;
    }

    public class DirectoryManager
    {
        String rootDirectory;
        public DirectoryInfo directoryInfoManager;
        DirectoryManagerException error;
        public DirectoryInfo[] subDirList;

        public String RootDirectory
        {
            get { return rootDirectory; }
            set {
                directoryInfoManager = new DirectoryInfo(value);
                if ((new DirectoryInfo(value)).Exists)
                {
                    rootDirectory = value;
                } else
                {
                    directoryInfoManager = new DirectoryInfo(rootDirectory);
                    error.text = value + " does not exists";
                }
            }
        }
        
        public bool CreateDirectory(String foldername)
        {
            try
            {
                DirectoryInfo result = directoryInfoManager.CreateSubdirectory(foldername);
                if (result.Exists)
                {
                    return true;
                }

            } catch(Exception e)
            {
                ;
            }
            return false;
        }

        public DirectoryManager()
        {
            rootDirectory = Directory.GetCurrentDirectory();
            directoryInfoManager = new DirectoryInfo(rootDirectory);
        }

        public DirectoryManager(String path)
        {
            directoryInfoManager = new DirectoryInfo(path);
            if (directoryInfoManager.Exists)
            {
                rootDirectory = path;
            } else
            {
                error.text = path + " does not exists";
                rootDirectory = Directory.GetCurrentDirectory();
                directoryInfoManager = new DirectoryInfo(rootDirectory);
            }
        }

        public int ReadDirectories()
        {
            subDirList =  directoryInfoManager.GetDirectories();
            return subDirList.Length;
        }

        public bool getFilFullPath(String directoryName, String filename, out String result)
        {
            ReadDirectories();
            result = "";

            foreach (var dir in subDirList)
            {
                if (dir.Name == directoryName)
                {
                    result = dir.FullName + "\\" + filename;
                    return true;
                }
            }
            return false;
        }
    }


    public interface I1CInterops
    {
        String GetItemData(params String[] list);
    }



    class MAIN_CLASS
    {
        static void Main(string[] args)
        {
            TsdServer srv = new TsdServer("10.1.4.88", 8888, "S:\\Отдел валидации\\Выгрузки");
            srv.RunServer();
        }

    }

    class TsdServer
    {
        String ServerIp;
        int Port;
        String rootDirectory; 

        Socket soc = null;
        PortManager busy_ports;

        IPAddress localAddr;
        DirectoryManager dirManager;
        //OneCConnector oneC;

        public TsdServer() { }
        public TsdServer(String ip, int Port, String Path)
        {
            ServerIp = ip;
            this.Port = Port;
            rootDirectory = Path;
            dirManager = new DirectoryManager(rootDirectory);
            busy_ports = new PortManager(50000, 50050);
        }

        
        public void RunServer()
        {
               if (IpAddressSetup(ServerIp))
               { 
                    soc = new Socket(localAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                    IPEndPoint ipEndPoint = new IPEndPoint(localAddr, 8888);
                    soc.Bind(ipEndPoint);
                    try
                    {
                        //oneC = new OneCConnector();
                        //if (oneC.ConnectToBase("Srvr='cl01-1c.pharmasyntez.com'; Ref='erp'; Usr='ПользовательОбмена';pwd='1pZNsQ';"))
                        //{
                            soc.Listen(50);
                            //SocketAsyncEventArgs AcceptArgs = new SocketAsyncEventArgs();
                            //AcceptArgs.Completed += AcceptCompleted;

                            while (true)
                            {
                        //soc.AcceptAsync(AcceptArgs);
                                Socket client = soc.Accept();
                                Task.Run(() => ProcessRequest(client));
                            }
                    //}
                    //else
                    //{
                    //    Console.WriteLine("Can't connecto to 1C server.");
                    //}
                } catch(Exception e)
                    {

                    } finally
                    {
                        if (soc.Connected)
                            soc.Close();
                    }
                } else
                {
                    Console.WriteLine("Ip address setting error.");
                }
        }

        void ProcessRequest(Socket s)
        {
            Console.WriteLine("Подключен клиент. Выполнение запроса...");

            try
            {

                    Console.WriteLine("Received data = {0}", s.Available);
                    byte[] inboundData = new byte[1024];  
                
                    s.Receive(inboundData);
                    String request = Encoding.UTF8.GetString(inboundData);
                    inboundData = null;
                    GC.Collect();

                    char[] supp_el = new char[1];
                    supp_el[0] = (char)0;
                    request = request.Trim(supp_el);
                    supp_el[0] = ':';
                    string[] input_list = request.Split(supp_el);

                    if (input_list[2] == "get folders")
                    {
                        ListDirectoryProcessor ldp = new ListDirectoryProcessor(input_list[1], s, dirManager);
                        ldp.Processing("");
                    }
                    else if (input_list[2] == "create folder")
                    {
                        CreateDirectoryProcessor cdp = new CreateDirectoryProcessor(input_list[1], s, dirManager);
                        cdp.Processing(input_list[3]);
                    }
                    else if (input_list[2] == "post file")
                    {
                        GetFileProcessor gfp = new GetFileProcessor(input_list[1], s, dirManager,localAddr,busy_ports);
                        gfp.Processing(input_list[3] + ";" + input_list[4] + ";" + input_list[5]);
                    }
            } catch (Exception e)
            {

            } finally
            {
                s.Close();
            }
        }

        private void AcceptCompleted(object sender, SocketAsyncEventArgs e)
        {
            object a = sender;
            //if (e.SocketError == SocketError.Success) //Если все норм с клиентом
            //{
            //    ClientConnection Client = new ClientConnection(e.AcceptSocket);
            //}
            //e.AcceptSocket = null;
            //AcceptAsync(AcceptAsyncArgs);
        }

        bool IpAddressSetup(String ip="127.0.0.1")
        {
            IPAddress[] allIpList = Dns.GetHostEntry(Dns.GetHostName()).AddressList;
            if (ip != "")
            {
                //IEnumerable<IPAddress> ipList = from lip in allIpList where (lip.AddressFamily == AddressFamily.InterNetwork && lip.Equals(ip) ) select lip;
                IEnumerable<IPAddress> ipList = from lip in allIpList where (lip.AddressFamily == AddressFamily.InterNetwork) select lip;
                if (ipList.Count() != 0)
                {
                    localAddr = IPAddress.Parse(ip);
                    return true;
                } else

                {
                    Console.WriteLine("There is no specified ip on this Host.");
                    return false;
                }
            } else
            {
                Console.WriteLine("Default '127.0.0.1' IP addres have been сonfigured.");
                return true;
            }
        }

    }


    

    public class OneCConnector : I1CInterops
    {
        V83.COMConnector Com = new V83.COMConnector();

        dynamic refer;
        dynamic query;

        public OneCConnector()
        {
  
        }

        public bool ConnectToBase(String ConnectionString)
        {
            try
            {
                refer = Com.Connect(ConnectionString);
                if (refer != null)
                {
                    query = refer.NewObject("Запрос");
                    query.Text = "выбрать т.номенклатура, т.характеристика, т.серия из регистрсведений.штрихкодыноменклатуры как т где т.штрихкод = &ШК";
                    return true;
                }
                else
                {
                    return false;
                }
            } catch(TSDException e)
            {
                return false;
            }
        }

        String I1CInterops.GetItemData(params string[] list)
        {
            query.УстановитьПараметр("ШК", list[0]);
            dynamic table = query.Execute().Unload();
            string response = list[0];

            for (int i = 0; i < table.Count(); i++)
            {
                for (int j = 0; j < table.Columns.Count(); j++)
                {
                    dynamic r1 = refer.String(table.Get(i).Get(j));
                    response += " " + r1;
                }
            }

            return response;
        }
    }



}
